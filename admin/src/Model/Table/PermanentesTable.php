<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Permanentes Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Permanente get($primaryKey, $options = [])
 * @method \App\Model\Entity\Permanente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Permanente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Permanente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Permanente|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Permanente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Permanente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Permanente findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PermanentesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('permanentes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [

            'afiche' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
            ],
            'miniatura' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'miniatura_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
            ],
            'portada' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'portada_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
            ],

        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('titulo')
            ->allowEmptyString('titulo');

        $validator
            ->scalar('descripcion')
            ->allowEmptyString('descripcion');

        $validator
            ->scalar('lugar')
            ->allowEmptyString('lugar');

        $validator
            ->scalar('direccion')
            ->allowEmptyString('direccion');

        $validator
            ->scalar('ubicacion')
            ->allowEmptyString('ubicacion');

        $validator
            ->scalar('hora')
            ->allowEmptyString('hora');

        $validator
            ->scalar('entrada')
            ->allowEmptyString('entrada');

        $validator
            ->scalar('latitud')
            ->allowEmptyString('latitud');

        $validator
            ->scalar('longitud')
            ->allowEmptyString('longitud');


        $validator
            ->scalar('dia_evento')
            ->allowEmptyString('dia_evento');

        $validator
            ->scalar('mes_evento')
            ->maxLength('mes_evento', 250)
            ->allowEmptyString('mes_evento');

        $validator
            ->scalar('anho_evento')
            ->maxLength('anho_evento', 250)
            ->allowEmptyString('anho_evento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
