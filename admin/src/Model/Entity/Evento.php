<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Evento Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $titulo
 * @property string|null $descripcion
 * @property string|null $lugar
 * @property string|null $direccion
 * @property string|null $ubicacion
 * @property string|null $miniatura
 * @property string|null $afiche
 * @property string|null $hora
 * @property string|null $destacado
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string|null $portada
 * @property string|null $latitud
 * @property string|null $longitud
 * @property string|null $foto_dir
 * @property string|null $miniatura_dir
 * @property string|null $portada_dir
 * @property string|null $dia_evento
 * @property string|null $mes_evento
 * @property string|null $año_evento
 * @property string|null $categoria
 * @property string|null $entrada
 * @property string|null $ultimo_dia
 * @property string|null $ultimo_mes
 * @property string|null $ultimo_año
 *
 * @property \App\Model\Entity\User $user
 */
class Evento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'titulo' => true,
        'descripcion' => true,
        'lugar' => true,
        'direccion' => true,
        'ubicacion' => true,
        'miniatura' => true,
        'afiche' => true,
        'hora' => true,
        'destacado' => true,
        'created' => true,
        'modified' => true,
        'portada' => true,
        'latitud' => true,
        'longitud' => true,
        'foto_dir' => true,
        'miniatura_dir' => true,
        'portada_dir' => true,
        'dia_evento' => true,
        'mes_evento' => true,
        'año_evento' => true,
        'categoria' => true,
        'entrada' => true,
        'ultimo_dia' => true,
        'ultimo_mes' => true,
        'ultimo_año' => true,
        'vistaprevia' => true,
        'dia_eliminar' => true,
        'mes_eliminar' => true,
        'ano_eliminar' => true,
        'user' => true
    ];
}
